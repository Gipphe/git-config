MAKE_DIR = make

.DEFAULT_GOAL = setup

## ----
## Main

# Core sets up some variables for us that are necessary in subsequent Makefiles,
# So we need to include it first.
include $(MAKE_DIR)/core/main.mk

MODULES := $(filter-out %core/main.mk,$(wildcard $(MAKE_DIR)/*/main.mk))
MODULES_TARGETS := $(MODULES:$(MAKE_DIR)/%/main.mk=$(STAMP)/setup-%)

include $(MODULES)

##   setup: Setup and install everything
.PHONY: setup
setup: $(MODULES_TARGETS)

