$(STAMP)/setup-installer-ruby: | $(STAMP)/
	touch $@

ifdef IS_MAC
$(STAMP)/gem-install-%: | $(STAMP)/install-ruby
	export PATH=$(BREW_BIN):$$PATH; \
	sudo gem install --user-install $(*)
	touch $@
else
$(STAMP)/gem-install-%: | $(STAMP)/install-ruby
	export PATH=$(BREW_BIN):$$PATH; \
	gem install --user-install $(*)
	touch $@
endif

ifdef IS_MAC
$(STAMP)/install-ruby: | $(STAMP)/
	$(info Ruby is already installed on Mac)
	touch $@
else
$(STAMP)/install-ruby: | $(BIF)-ruby
	touch $@
endif

GEM = $(STAMP)/gem-install
