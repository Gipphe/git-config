$(STAMP)/setup-installer-coursier: | $(STAMP)/install-coursier
	touch $@

CS_INSTALL = $(STAMP)/coursier-install

$(STAMP)/coursier-install-%: | $(STAMP)/install-coursier
	PATH=$(BREW_BIN):$$PATH coursier install $(*)
	touch $@
