$(STAMP)/setup-installer-brew: | $(STAMP)/install-brew
	touch $@

BREW = $(BREW_BIN)/brew

$(STAMP)/install-brew: | $(STAMP)/install-brew-script
	touch $@

ifdef IS_LINUX
$(STAMP)/install-brew-script: | $(STAMP)/apt-install-build-essential
endif

ifdef IS_LINUX
$(STAMP)/install-brew-deps: | $(STAMP)/apt-install-build-essential $(STAMP)/apt-install-curl
	touch $@
else
$(STAMP)/install-brew-deps: | $(STAMP)/
	touch $@
endif

$(STAMP)/install-brew-script: | $(STAMP)/install-brew-deps
	export NONINTERACTIVE=1 && \
	/bin/bash -c "$$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
	touch $@

BIF = $(STAMP)/brew-install-formula

$(STAMP)/brew-install-formula-%: | $(STAMP)/install-brew
	$(BREW) install --formula $*
	touch $@

BIC = $(STAMP)/brew-install-cask
ifdef IS_MAC
$(STAMP)/brew-install-cask-%: | $(STAMP)/install-brew
	$(BREW) install --cask $*
	touch $@
endif
