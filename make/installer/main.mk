INSTALLER_MODULES = $(filter-out %main.mk,$(wildcard $(MAKE_DIR)/installer/*.mk))
INSTALLER_TARGETS = $(INSTALLER_MODULES:$(MAKE_DIR)/installer/%.mk=$(STAMP)/setup-installer-%)

include $(INSTALLER_MODULES)

$(STAMP)/setup-installer: | $(INSTALLER_TARGETS)
	touch $@

