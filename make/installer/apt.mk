$(STAMP)/setup-installer-apt: | $(STAMP)/
	touch $@

$(STAMP)/apt-update: | $(STAMP)/
	sudo apt update
	touch $@

$(STAMP)/apt-install-%: | $(STAMP)/apt-update
	sudo apt install -y $(*)
	touch $@

