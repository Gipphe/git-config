$(STAMP)/setup-installer-nix: | $(STAMP)/configure-nix $(STAMP)/install-nix
	touch $@

ifdef IS_MAC
$(STAMP)/install-nix: | $(STAMP)/install-curl
	curl -L https://nixos.org/nix/install | sh
	touch $@
else
$(STAMP)/install-nix: | $(STAMP)/install-curl
	curl -L https://nixos.org/nix/install | sh -s -- --daemon
	touch $@
endif

NIX_CONF = /etc/nix/nix.conf

$(STAMP)/configure-nix: | $(STAMP)/install-nix
	@if [[ -z $$(egrep 'nix-command|flakes' $(NIX_CONF)) ]]; then \
		echo 'Setting experimental features in nix.conf'; \
		echo '' | sudo tee -a $(NIX_CONF) 1>/dev/null; \
		echo 'experimental-features = nix-command flakes' | sudo tee -a $(NIX_CONF) 1>/dev/null; \
	else \
		echo 'Experimental features already enabled.'; \
	fi
	touch $@
