$(STAMP)/setup-programs-gh: | \
  $(STAMP)/install-gh \
  $(HOME)/.config/gh/config.yml
	touch $@

$(STAMP)/install-gh: | $(BIF)-gh
	touch $@

$(HOME)/.config/gh/config.yml: | config/gh/config.yml $(HOME)/.config/gh
	ln -s $$(pwd)/$(firstword $|) $@ 

$(HOME)/.config/gh:
	mkdir -p $@
