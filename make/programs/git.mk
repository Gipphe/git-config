$(STAMP)/setup-programs-git: | $(HOME)/.gitconfig $(HOME)/.gitconfig.strise $(CONFIG)/git/ignore
	touch $@

$(HOME)/.gitconfig: | config/git/.gitconfig
	ln -s $$(pwd)/$(firstword $|) $@

$(HOME)/.gitconfig.strise: | config/git/.gitconfig.strise $(HOME)/.gitconfig
	ln -s $$(pwd)/$(firstword $|) $@

$(CONFIG)/git/ignore: | config/git/.gitignore $(CONFIG)/git/
	ln -s $$(pwd)/$(firstword $|) $@

$(CONFIG)/git:
	mkdir -p $@
