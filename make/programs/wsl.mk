ifdef IS_WSL
$(STAMP)/setup-programs-wsl: | $(STAMP)/ /etc/wsl.conf
	touch $@
else
$(STAMP)/setup-programs-wsl: | $(STAMP)/
	touch $@
endif

/etc/wsl.conf: config/wsl.conf
	sudo cp $< $@
