PROGRAMS_MODULES = $(filter-out %main.mk,$(wildcard $(MAKE_DIR)/programs/*.mk))
PROGRAMS_TARGETS = $(PROGRAMS_MODULES:$(MAKE_DIR)/programs/%.mk=$(STAMP)/setup-programs-%)

include $(PROGRAMS_MODULES)

$(STAMP)/setup-programs: | $(PROGRAMS_TARGETS)
	touch $@

