$(STAMP)/setup-programs-ssh: | \
  $(SSH)/config \
  $(SSH)/github.ssh \
	$(SSH)/gitlab.ssh \
	$(SSH)/codeberg.ssh \
	$(SSH)/known_hosts
	touch $@

$(SSH)/config: | config/ssh/ssh.conf
	ln -s $$(pwd)/$(firstword $|) $@

$(SSH)/%.ssh:
	ssh-keygen -t ed25519 -a 100 -C $(*) -f $@

$(SSH)/known_hosts: | config/ssh/known_hosts
	ln -s $$(pwd)/$(firstword $|) $@
