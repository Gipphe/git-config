ifdef IS_MAC
$(STAMP)/setup-programs-barrier: | \
  $(STAMP)/install-barrier \
  $(HOME)/Library/Application\ Support/barrier/SSL/Barrier.pem \
  $(HOME)/Library/Application\ Support/barrier/SSL/Fingerprints/Local.txt
	touch $@
else
$(STAMP)/setup-programs-barrier: | \
  /mnt/c/Users/Gipphe/AppData/Local/Barrier/SSL/Barrier.pem \
  $(STAMP)/
	touch $@
endif

ifdef IS_MAC
$(STAMP)/install-barrier: | $(BIC)-barrier
endif

$(HOME)/Library/Application\ Support/barrier/SSL/Barrier.pem:
	openssl req -x509 -nodes -days 365 -subj /CN=Barrier -newkey rsa:4096 -keyout "$@" -out "$@"

$(HOME)/Library/Application\ Support/barrier/SSL/Fingerprints/Local.txt: $(HOME)/Library/Application\ Support/barrier/SSL/Barrier.pem
	fingerprint=$$(openssl x509 -fingerprint -sha256 -noout -in "$<" | cut -d"=" -f2); \
	echo "v2:sha256:$$fingerprint" > "$@"

/mnt/c/Users/Gipphe/AppData/Local/Barrier/SSL/Barrier.pem:
	openssl req -x509 -nodes -days 365 -subj /CN=Barrier -newkey rsa:4096 -keyout '$@' -out '$@'
