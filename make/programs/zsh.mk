$(STAMP)/setup-programs-zsh: | \
  $(STAMP)/install-zsh \
  $(HOME)/.zshrc \
  $(OMZ) \
  $(OMZ)/custom/themes/headline \
  $(STAMP)/set-default-shell
	touch $@

ifdef IS_MAC
$(STAMP)/install-zsh: | $(STAMP)/
	$(info Mac already comes with zsh by default)
	touch $@

ZSH_PATH = /bin/zsh
else
$(STAMP)/install-zsh: | $(STAMP)/apt-update
	sudo apt install -y zsh
	touch $@

ZSH_PATH = /usr/bin/zsh
endif

$(HOME)/.zshrc: | config/.zshrc
	ln -s $$(pwd)/$(firstword $|) $@

$(OMZ): | $(HOME)/.zshrc $(STAMP)/brew-install-formula-curl $(STAMP)/install-zsh
	export RUNZSH='no'; \
	export PATH=$(BREW_BIN):$$PATH; \
	export KEEP_ZSHRC='yes'; \
	curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh | zsh

$(OMZ)/custom/themes/headline: | $(STAMP)/install-git $(OMZ)
	git clone https://github.com/moarram/headline.git $(OMZ)/custom/themes/headline

ifdef IS_MAC
$(STAMP)/set-default-shell: | $(STAMP)/
	$(info Not necessary to replace shell on Mac)
	touch $@
else
$(STAMP)/set-default-shell: | $(STAMP)/install-zsh
	if grep -vq '$(ZSH_PATH)' /etc/shells; then echo '$(ZSH_PATH)' | sudo tee -a /etc/shells; fi; \
	chsh -s $(ZSH_PATH) $$USER
	touch $@
endif
