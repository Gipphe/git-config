$(STAMP)/setup-programs-vim: | $(HOME)/.vimrc
	touch $@

$(HOME)/.vimrc: | config/.vimrc
	ln -s $$(pwd)/$(firstword $|) $@

