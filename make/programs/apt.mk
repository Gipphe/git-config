ifdef IS_MAC
$(STAMP)/setup-programs-apt: | $(STAMP)/
	touch $@
else
$(STAMP)/setup-programs-apt: | \
  $(STAMP)/apt-install-xdg-utils
	touch $@
endif
