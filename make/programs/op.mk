ifdef IS_MAC
$(STAMP)/setup-programs-op: | $(BIC)-1password-cli
	touch $@
else
$(STAMP)/setup-programs-op: | $(STAMP)/install-1password-cli
	touch $@
endif

/usr/share/keyrings/1password-archive-keyring.gpg: | $(STAMP)/install-curl
	curl -sS https://downloads.1password.com/linux/keys/1password.asc | \
	sudo gpg --dearmor --output $@

/etc/apt/sources.list.d/1password.list:
	echo "deb [arch=$$(dpkg --print-architecture) signed-by=/usr/share/keyrings/1password-archive-keyring.gpg] https://downloads.1password.com/linux/debian/$$(dpkg --print-architecture) stable main" | \
	sudo tee /etc/apt/sources.list.d/1password.list

/etc/debsig/policies/AC2D62742012EA22/1password.pol: | $(STAMP)/install-curl
	sudo mkdir -p $(@D)
	curl -sS https://downloads.1password.com/linux/debian/debsig/1password.pol | \
	sudo tee $@

/usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg:
	sudo mkdir -p $(@D)
	curl -sS https://downloads.1password.com/linux/keys/1password.asc | \
	sudo gpg --dearmor --output $@

$(STAMP)/install-1password-cli: | \
  $(STAMP)/ \
  /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg \
  /etc/debsig/policies/AC2D62742012EA22/1password.pol \
  /etc/apt/sources.list.d/1password.list \
  /usr/share/keyrings/1password-archive-keyring.gpg
	sudo apt update
	sudo apt install 1password-cli
	touch $@
