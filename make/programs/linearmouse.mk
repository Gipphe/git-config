ifdef IS_MAC
$(STAMP)/setup-programs-linearmouse: | $(STAMP)/install-linearmouse $(HOME)/.config/linearmouse/linearmouse.json
	touch $@
else
$(STAMP)/setup-programs-linearmouse: | $(STAMP)/
	touch $@
endif

$(STAMP)/install-linearmouse: | $(BIC)-linearmouse
	touch $@

$(HOME)/.config/linearmouse/linearmouse.json: | config/linearmouse.json $(STAMP)/install-linearmouse
	ln -s $$(pwd)/$(firstword $|) $@
