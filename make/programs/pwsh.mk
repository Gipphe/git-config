$(STAMP)/setup-programs-pwsh: | $(STAMP)/install-pwsh
	touch $@

ifdef IS_MAC
$(STAMP)/install-pwsh: | $(STAMP)/install-brew
	PATH=$(BREW_BIN):$$PATH brew install --formula powershell/tap/powershell
	touch $@
else
$(STAMP)/install-pwsh: | $(STAMP)/install-curl $(STAMP)/apt-install-libicu72
	f=$$(mktemp); \
	curl -sSLo $$f https://github.com/PowerShell/PowerShell/releases/download/v7.3.9/powershell_7.3.9-1.deb_amd64.deb; \
	sudo dpkg -i $$f; \
	sudo apt install -f; \
	rm -f $$f
	touch $@
endif
