FISH_DIR = $(CONFIG)/fish
fish_functions = $(wildcard config/fish/functions/*)

$(STAMP)/setup-programs-fish: | \
  $(STAMP)/set-fish-as-shell \
  $(FISH_DIR)/config.fish \
	$(FISH_DIR)/functions/tide.fish \
	$(FISH_DIR)/functions/fisher.fish \
	$(FISH_DIR)/functions/bass.fish \
	$(FISH_DIR)/conf.d/nix.fish \
	$(fish_functions:config/fish/functions/%=$(FISH_DIR)/functions/%) \
	$(STAMP)/configure-tide-prompt
	touch $@

$(STAMP)/set-fish-as-shell: | $(STAMP)/add-fish-to-allowed-shells
	PATH=$(BREW_BIN):$$PATH	chsh -s $$(command -v fish)
	touch $@

$(STAMP)/add-fish-to-allowed-shells: | $(BIF)-fish
	PATH=$(BREW_BIN):$$PATH command -v fish | sudo tee -a /etc/shells
	touch $@

$(FISH_DIR)/config.fish: | config/fish/config.fish $(FISH_DIR)/
	ln -s $$(pwd)/$(firstword $|) $@

$(FISH_DIR)/functions/fisher.fish $(FISH_DIR)/completions/fisher.fish: | \
  $(STAMP)/install-curl \
  $(BIF)-fish
	export PATH=$(BREW_BIN):$$PATH; \
	fish --no-config -c 'curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source && fisher install jorgebucaran/fisher'

$(FISH_DIR)/functions/tide.fish: | $(FISH_DIR)/functions/fisher.fish
	fish -c 'fisher install ilancosman/tide@v6'

$(FISH_DIR)/functions/bass.fish: | $(FISH_DIR)/functions/fisher.fish
	fish -c 'fisher install edc/bass'

$(FISH_DIR)/conf.d/nix.fish: | $(FISH_DIR)/functions/fisher.fish
	fish -c 'fisher install kidonng/nix.fish'

$(STAMP)/configure-tide-prompt: | $(FISH_DIR)/functions/fisher.fish $(FISH_DIR)/functions/tide.fish
	fish -c '\
		tide configure \
				--auto \
				--style=Lean \
				--prompt_colors="True color" \
				--show_time="24-hour format" \
				--lean_prompt_height="Two lines" \
				--prompt_connection=Solid \
				--prompt_connection_andor_frame_color=Dark \
				--prompt_spacing=Sparse \
				--icons="Few icons" \
				--transient=No \
	'
	touch $@

$(FISH_DIR)/:
	mkdir -p $@

$(FISH_DIR)/functions/%: | config/fish/functions/%
	mkdir -p $(@D)
	ln -s $$(pwd)/$(firstword $|) $@
