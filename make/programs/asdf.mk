$(STAMP)/setup-programs-asdf: \
  $(STAMP)/install-nodejs \
  $(STAMP)/asdf-install-terraform-1.5.7
	touch $@

plugin_name = $(word 1,$(subst -, ,$(*)))
plugin_version = $(word 2,$(subst -, ,$(*)))

$(STAMP)/asdf-install-%: | $(STAMP)/install-asdf $(STAMP)/install-unzip
	PATH=$(BREW_BIN):$$PATH asdf plugin add $(call plugin_name,$(*))
	PATH=$(BREW_BIN):$$PATH asdf install $(call plugin_name,$(*)) $(call plugin_version,$(*))
	PATH=$(BREW_BIN):$$PATH asdf global $(call plugin_name,$(*)) $(call plugin_version,$(*))
	touch $@

$(STAMP)/install-nodejs: | $(STAMP)/asdf-install-nodejs-20.9.0
	touch $@

