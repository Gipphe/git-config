$(STAMP)/setup-programs-brew: | $(STAMP)/setup-formulae $(STAMP)/setup-casks
	touch $@

define BREW_FORMULAE_LIST
asdf
difftastic
direnv
entr
fastgron
fd
gh
git
glab
gnupg
hurl
imagemagick
jq
lazygit
nvim
openssh
pipx
poetry
python
ripgrep
thefuck
tmux
wget
endef

ifdef IS_MAC
BREW_FORMULAE_LIST += jj
BREW_FORMULAE_LIST += libiconv
endif

BREW_FORMULAE = $(subst $(NEWLINE),$(SPACE),$(BREW_FORMULAE_LIST))

define BREW_CASKS_LIST
1password
1password-cli
alt-tab
cyberduck
docker
filen
font-fira-code-nerd-font
git-credential-manager
karabiner-elements
kitty
linearmouse
notion
obsidian
slack
spotify
vscodium
endef

BREW_CASKS = $(subst $(NEWLINE),$(SPACE),$(BREW_CASKS_LIST))

BREW_FORMULA_TARGETS = $(BREW_FORMULAE:%=$(BIF)-%)
BREW_CASK_TARGETS = $(BREW_CASKS:%=$(BIC)-%)

$(STAMP)/setup-formulae: | $(BREW_FORMULA_TARGETS)
	touch $@

ifdef IS_MAC
$(STAMP)/setup-casks: | $(BREW_CASK_TARGETS)
else
$(STAMP)/setup-casks: | $(STAMP)/
	@echo "Casks do not work on linux"
	touch $@
endif
