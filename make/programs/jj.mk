$(STAMP)/setup-programs-jj: | $(STAMP)/install-jujutsu $(HOME)/.jjconfig.toml
	touch $@

ifdef IS_MAC
$(STAMP)/install-jujutsu: | $(BIF)-jj
	touch $@
else
$(STAMP)/install-jujutsu: | $(STAMP)/install-jujutsu-bin
	touch $@

$(STAMP)/install-jujutsu-bin: | \
 $(STAMP)/install-rust \
 $(STAMP)/apt-install-libssl-dev \
 $(STAMP)/apt-install-openssl \
 $(STAMP)/apt-install-pkg-config \
 config/ssh/ssh-agent.sh \
 $(SSH)/github.ssh
	source $$HOME/.cargo/env && \
	source ./config/ssh/ssh-agent.sh && \
	init_ssh_agent && \
	cargo install --git ssh://git@github.com/martinvonz/jj.git --locked --bin jj jj-cli
	touch $@
endif

$(HOME)/.jjconfig.toml: | config/.jjconfig.toml
	ln -s $$(pwd)/$(firstword $|) $@
