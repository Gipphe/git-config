$(STAMP)/setup-programs-misc: | \
  $(STAMP)/install-codeberg-cli
	touch $@

$(STAMP)/install-asdf: | $(BIF)-asdf
	touch $@

ifdef USE_BREW
$(STAMP)/install-rust: | $(BIF)-rustup-init
	PATH=$(BREW_BIN):$$PATH rustup-init -y
	touch $@
endif

$(STAMP)/install-git: | $(BIF)-git
	touch $@

ifdef USE_BREW
$(STAMP)/install-curl: | $(BIF)-curl
	touch $@
else
$(STAMP)/install-curl: | $(STAMP)/apt-install-curl
	touch $@
endif

ifdef USE_BREW
$(STAMP)/install-zip: | $(BIF)-zip
	touch $@
else
$(STAMP)/install-zip: | $(STAMP)/apt-install-zip
	touch $@
endif

ifdef USE_BREW
$(STAMP)/install-unzip: | $(BIF)-unzip
	touch $@
else
$(STAMP)/install-unzip: | $(STAMP)/apt-install-unzip
	touch $@
endif

ifdef USE_BREW
$(STAMP)/install-fd: | $(BIF)-fd
	touch $@
else
$(STAMP)/install-fd: | $(STAMP)/apt-install-fd
	touch $@
endif

$(STAMP)/install-ripgrep: | $(BIF)-ripgrep
	touch $@

$(STAMP)/install-codeberg-cli: | $(STAMP)/install-rust $(SSH)/codeberg.ssh config/ssh/ssh-agent.sh
	source $$HOME/.cargo/env && source ./config/ssh/ssh-agent.sh && init_ssh_agent && cargo install codeberg-cli
	touch $@

$(STAMP)/sed: | $(BIF)-gsed
	touch $@
