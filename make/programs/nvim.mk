$(STAMP)/setup-programs-nvim: | \
  $(BIF)-nvim \
	$(HOME)/.config/nvim \
	$(HOME)/.config/nvim-kickstart \
	$(STAMP)/install-nvim \
	$(STAMP)/install-ripgrep \
	$(STAMP)/install-fd
	touch $@

$(STAMP)/install-nvim: | $(BIF)-nvim $(STAMP)/install-nvim-node-host
	touch $@

$(HOME)/.config/nvim: | config/nvim
	ln -s $$(pwd)/$(firstword $|) $@

$(HOME)/.config/nvim-kickstart: | config/nvim-kickstart
	ln -s $$(pwd)/$(firstword $|) $@

$(STAMP)/install-nvim-node-host: | $(STAMP)/install-nodejs
	PATH=$(BREW_BIN):$$PATH npm i -g neovim
	touch $@
