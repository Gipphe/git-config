$(STAMP)/setup-programs-haskell: | \
  $(STAMP)/install-ghcup \
  $(STAMP)/ghcup-install-stack \
	$(STAMP)/ghcup-install-hls \
  $(HOME)/.stack/hooks/ghc-install.sh \
	$(STAMP)/stack-install-fast-tags \
	$(STAMP)/cabal-install-haskell-debug-adapter \
	$(STAMP)/cabal-install-haskell-dap \
	$(STAMP)/cabal-install-ghci-dap
	touch $@

ifdef IS_LINUX
GHCUP_DEPS = build-essential curl libffi-dev libffi8ubuntu1 libgmp-dev libgmp10 libncurses-dev libncurses5 libtinfo5
$(BIF)-ghcup: | $(GHCUP_DEPS:%=$(STAMP)/apt-install-%)
endif

$(STAMP)/install-ghcup: | $(BIF)-ghcup
	touch $@

$(STAMP)/ghcup-install-%: | $(STAMP)/install-ghcup
	export PATH=$(BREW_BIN):$$PATH; \
	ghcup install $(*); \
	ghcup set $(*)
	touch $@

$(STAMP)/install-cabal: | $(STAMP)/ghcup-install-cabal
	PATH=$(GHCUP_BIN):$$PATH cabal update
	touch $@

$(STAMP)/install-stack: | $(STAMP)/ghcup-install-stack
	touch $@

$(STAMP)/install-ghc: | $(STAMP)/ghcup-install-ghc
	touch $@

ifdef IS_LINUX
HLS_DEPS = libicu-dev libncurses-dev libgmp-dev zlib1g-dev
$(STAMP)/ghcup-install-hls: | $(HLS_DEPS:%=$(STAMP)/apt-install-%)
endif

GHCUP_BIN = $(HOME)/.ghcup/bin

$(HOME)/.stack/hooks/ghc-install.sh: | $(STAMP)/install-curl $(STAMP)/install-stack
	mkdir -p $(HOME)/.stack/hooks
	PATH=$(BREW_BIN):$$PATH curl -sSLo '$@' https://raw.githubusercontent.com/haskell/ghcup-hs/master/scripts/hooks/stack/ghc-install.sh
	chmod +x '$@'
	$(GHCUP_BIN)/stack config set system-ghc false --global

$(STAMP)/stack-install-%: | $(STAMP)/install-stack
	export PATH=$(BREW_BIN):$(GHCUP_BIN):$$PATH; \
	stack install $(*)
	touch $@

$(STAMP)/cabal-install-%: | $(STAMP)/install-cabal $(STAMP)/install-ghc
	export PATH=$(BREW_BIN):$(GHCUP_BIN):$$PATH; \
	cabal install $(*)
	touch $@

$(STAMP)/cabal-install-haskell-debug-adapter: | $(STAMP)/cabal-install-haskell-dap $(STAMP)/cabal-install-ghci-dap
