ifdef IS_LINUX
$(STAMP)/setup-programs-man: | $(STAMP)/apt-install-man-db
	touch $@
else
$(STAMP)/setup-programs-man: | $(STAMP)/
	touch $@
endif
