ifdef IS_WSL
$(STAMP)/setup-programs-kitty: | $(STAMP)/
	$(info No kitty for you in WSL)
	touch $@
else
$(STAMP)/setup-programs-kitty: | \
  $(STAMP)/install-kitty \
	$(STAMP)/configure-kitty
	touch $@
endif

$(STAMP)/install-kitty: | $(BIC)-kitty
	touch $@

$(STAMP)/configure-kitty: | $(HOME)/.config/kitty/kitty.conf $(STAMP)/install-kitty
	touch $@

$(HOME)/.config/kitty/kitty.conf: | config/kitty.conf $(STAMP)/install-kitty $(HOME)/.config/kitty/kitty-themes
	ln -s $$(pwd)/$(firstword $|) $@

$(HOME)/.config/kitty/kitty-themes: | $(STAMP)/install-git
	git clone --depth 1 https://github.com/dexpota/kitty-themes.git $@

