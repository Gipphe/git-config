ifdef IS_WSL
$(STAMP)/setup-programs-wslu: | $(STAMP)/apt-install-wslu $(STAMP)/register-wslview-as-browser
	touch $@
else
$(STAMP)/setup-programs-wslu: | $(STAMP)/
	touch $@
endif

$(STAMP)/register-wslview-as-browser: | $(STAMP)/apt-install-wslu
	wslview --reg-as-browser
	touch $@

$(STAMP)/apt-install-wslu: | $(STAMP)/wslu-prereqisites

$(STAMP)/wslu-prereqisites: | \
  /etc/apt/sources.list.d/wslu.list \
  /etc/apt/trusted.gpg.d/wslu.asc
	sudo apt update
	touch $@

/etc/apt/sources.list.d/wslu.list:
	echo "deb https://pkg.wslutiliti.es/debian $(OS_RELEASE_CODENAME) main" | sudo tee $@

/etc/apt/trusted.gpg.d/wslu.asc: | \
  $(STAMP)/apt-install-gnupg2 \
  $(STAMP)/apt-install-apt-transport-https
	curl https://pkg.wslutiliti.es/public.key | sudo tee -a $@
