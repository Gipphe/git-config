ifdef IS_MAC
$(STAMP)/setup-programs-llamafile: \
  $(HOME)/.local/bin/llava.llamafile \
  $(HOME)/.local/bin/mistral.llamafile
#  $(HOME)/.local/bin/wizardcoder.llamafile
	touch $@
else ifdef IS_WSL
$(STAMP)/setup-programs-llamafile: | $(STAMP)/
	$(info llamafiles in WSL require more setup)
	touch $@
else ifdef IS_LINUX
$(STAMP)/setup-programs-llamafile: | $(STAMP)/
	$(info llamafiles on Linux require more setup)
	touch $@
else
$(STAMP)/setup-programs-llamafile: | $(STAMP)/
	$(info llamafiles require additional setup on whatever system this is)
	touch $@
endif

$(HOME)/.local/bin/llava.llamafile: | $(STAMP)/install-curl $(HOME)/.local/bin/
	curl -Lo $@ https://huggingface.co/jartine/llava-v1.5-7B-GGUF/resolve/main/llava-v1.5-7b-q4-main.llamafile?download=true
	chmod +x $@

$(HOME)/.local/bin/mistral.llamafile: | $(STAMP)/install-curl $(HOME)/.local/bin/
	curl -Lo $@ https://huggingface.co/jartine/mistral-7b.llamafile/resolve/main/mistral-7b-instruct-v0.1-Q4_K_M-main.llamafile?download=true
	chmod +x $@

$(HOME)/.local/bin/wizardcoder.llamafile: | $(STAMP)/install-curl $(HOME)/.local/bin/
	curl -Lo $@ https://huggingface.co/jartine/wizardcoder-13b-python/resolve/main/wizardcoder-python-13b-main.llamafile?download=true
	chmod +x $@
