TMUXINATOR_CONFIGS = $(wildcard config/tmuxinator/*.yml)

$(STAMP)/setup-programs-tmux: | \
  $(STAMP)/install-tmuxinator-completion \
  $(STAMP)/install-tmuxinator \
  $(STAMP)/install-tmux \
  $(STAMP)/install-tmux-plugin-manager \
  $(TMUXINATOR_CONFIGS:config/tmuxinator/%.yml=$(CONFIG)/tmuxinator/%.yml) \
  $(STAMP)/configure-tmux
	touch $@

ifdef USE_BREW
$(STAMP)/install-tmux: | $(BIF)-tmux
	touch $@
endif

ifdef IS_MAC
$(STAMP)/configure-tmux: $(CONFIG)/tmux/tmux.conf | $(STAMP)/install-tmux $(BIF)-reattach-to-user-namespace
	touch $@
else
$(STAMP)/configure-tmux: $(CONFIG)/tmux/tmux.conf | $(STAMP)/install-tmux
	touch $@
endif

$(CONFIG)/tmux/tmux.conf: config/tmux/tmux.conf | $(CONFIG)/tmux/
	cp $< $@

$(CONFIG)/tmux/:
	mkdir -p $@

$(STAMP)/install-tmuxinator: | $(GEM)-tmuxinator
	touch $@

$(STAMP)/install-tmuxinator-completion: | /usr/local/share/zsh/site-functions/_tmuxinator
	touch $@

/usr/local/share/zsh/site-functions/_tmuxinator: | $(STAMP)/install-curl /usr/local/share/zsh/site-functions/
	export PATH=$(BREW_BIN):$$PATH; \
	sudo curl -sSLo '$@' https://raw.githubusercontent.com/tmuxinator/tmuxinator/master/completion/tmuxinator.zsh

/usr/local/share/zsh/site-functions/:
	sudo mkdir -p $@

$(STAMP)/install-tmux-plugin-manager: | $(HOME)/.tmux/plugins/tpm

$(HOME)/.tmux/plugins/tpm: | $(STAMP)/install-git
	PATH=$(BREW_BIN):$$PATH git clone https://github.com/tmux-plugins/tpm $@

$(CONFIG)/tmuxinator/%.yml: config/tmuxinator/%.yml | $(CONFIG)/tmuxinator/
	cp $< $@
