## -----
## Utils

##   print-%: Print a variable, replacing % with the variable name you want to
##          : inspect.
.PHONY: print-%
print-%:
	@echo $* = $($*)

##   help: Show this help text.
.PHONY: help
help: | $(STAMP)/sed
	@$(SED) -n 's/^##//p' $(MAKEFILE_LIST) | column -t -s:

##   tear-down: Delete and remove the $(STAMP) folder, effectively resetting
##            : the state of this setup.
.PHONY: tear-down
tear-down:
	rm -rf $(STAMP)
