SHELL = bash
SHELL_CONFIG = $(HOME)/.zshrc
.SHELLFLAGS = -ec -o pipefail

NULL =
SPACE = $(NULL) $(NULL)
define NEWLINE


endef

SSH = $(HOME)/.ssh
OMZ = $(HOME)/.oh-my-zsh
CONFIG = $(HOME)/.config

CWD := $(shell pwd)
STAMP := $(CWD)/STAMP

_IS_MAC = $(shell if [ "$$(uname)" = 'Darwin' ]; then echo "yes"; else echo ""; fi)
ifneq ($(_IS_MAC),)
IS_MAC = yes
endif

_IS_LINUX = $(shell if [ "$$(uname)" = 'Linux' ]; then echo "yes"; else echo ""; fi)
ifneq ($(_IS_LINUX),)
IS_LINUX = yes
endif

_IS_WSL = $(shell if [ ! -z $$(grep -E 'microsoft|WSL' /proc/sys/kernel/osrelease 2>/dev/null) ]; then echo "1"; else echo ""; fi)
ifneq ($(_IS_WSL),)
IS_WSL = yes
endif

_IS_HOME = $(shell if [ "$$(jq '.ENV == "home"' config.json)" = 'true' ]; then echo "yes"; else echo ""; fi)
ifneq ($(_IS_HOME),)
IS_HOME = yes
endif

MACOS_VERSION = $(shell sw_vers -productVersion)
MACOS_VERSION_NAME = $(shell awk '/SOFTWARE LICENSE AGREEMENT FOR macOS/' '/System/Library/CoreServices/Setup Assistant.app/Contents/Resources/en.lproj/OSXSoftwareLicense.rtf' | awk -F 'macOS ' '{print $$NF}' | awk '{print substr($$0, 0, length($$0)-1)}')

ifdef IS_MAC
OS_DISTRO = $(shell uname)
OS_RELEASE_CODENAME = $(MACOS_VERSION_NAME)
OS_RELEASE_VERSION = $(shell sw_vers -productVersion)
else
OS_DISTRO = $(shell source /etc/os-release; echo $$NAME)
OS_RELEASE_CODENAME = $(shell source /etc/os-release; echo $$VERSION_CODENAME)
OS_RELEASE_VERSION = $(shell source /etc/os-release; echo $$VERSION_ID)
endif

SETUP_CONFIG = config.json

USE_BREW = $(IS_MAC)$(IS_LINUX)
ifdef IS_MAC
SED = gsed
else
SED = sed
endif

ifdef USE_BREW
PIP = pipx
else
PIP = pip
endif

ifdef IS_MAC
BREW_BIN ?= /opt/homebrew/bin
else
BREW_BIN ?= /home/linuxbrew/.linuxbrew/bin
endif

escape_path = $(subst .,\.,$(subst /,\/,$(1)))

$(STAMP):
	mkdir -m 700 -p $@

.PRECIOUS: %/
%/:
	mkdir -p $@

include $(MAKE_DIR)/core/utils.mk

$(STAMP)/setup-core: | $(STAMP)/
	touch $@
