_IS_STRISE = $(shell if [ "$$(jq -r '.ENV' config.json)" = 'strise' ]; then echo '1'; else echo ''; fi)
ifneq ($(_IS_STRISE),)
IS_STRISE = yes
endif

STRISE_MODULES = $(filter-out %main.mk,$(wildcard $(MAKE_DIR)/strise/*.mk))
STRISE_TARGETS = $(STRISE_MODULES:$(MAKE_DIR)/strise/%.mk=$(STAMP)/setup-strise-%)

include $(STRISE_MODULES)

ifdef IS_STRISE 
$(STAMP)/setup-strise: | $(STRISE_TARGETS)
	touch $@
else
$(STAMP)/setup-strise: | $(STAMP)/
	@echo "This is not a Strise machine. Skipping Strise-specific setup."
	touch $@
endif

ifdef IS_STRISE
TEST = "yes"
else
TEST = "no"
endif
