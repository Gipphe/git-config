SDKMAN_SDKS =
SDKMAN_SDKS += scala-2.12.17
SDKMAN_SDKS += java-17.0.8-amzn

$(STAMP)/setup-strise-sdkman: | \
  $(STAMP)/install-sdkman \
	$(SDKMAN_SDKS:%=$(STAMP)/sdk-install-%) \
	$(CONFIG)/fish/functions/sdk.fish
	touch $@

$(STAMP)/install-sdkman: | $(STAMP)/install-curl $(STAMP)/install-zip $(STAMP)/install-unzip
	curl -s "https://get.sdkman.io" | bash
	touch $@

sdk_name = $(word 1,$(subst -, ,$(*)))
sdk_version = $(subst $(SPACE),-,$(strip $(wordlist 2,$(words $(subst -, ,$(*))),$(subst -, ,$(*)))))

$(STAMP)/sdk-install-%: | $(STAMP)/install-sdkman
	source $(HOME)/.sdkman/bin/sdkman-init.sh && \
	sdk install $(call sdk_name,$(*)) $(call sdk_version,$(*)) && \
	sdk default $(call sdk_name,$(*)) $(call sdk_version,$(*))
	touch $@


$(CONFIG)/fish/conf.d/sdk.fish $(CONFIG)/fish/functions/sdk.fish $(CONFIG)/fish/completions/sdk.fish: | $(CONFIG)/fish/functions/fisher.fish
	fish -c 'fisher install reitzig/sdkman-for-fish@v2.0.0'

