$(STAMP)/setup-strise-gcloud: | \
	$(STAMP)/install-gcloud \
	$(STAMP)/configure-kubectx \
	$(STAMP)/configure-docker-for-gcloud
	touch $@

GCLOUD_ROOT = $(HOME)/google-cloud-sdk

COMMON_NS =
COMMON_NS += jenkins
COMMON_NS += shared
COMMON_NS += janitor
COMMON_NS += knowledge

GCP_PROJECTS =
GCP_PROJECTS += strise-common
GCP_PROJECTS += strise-stage
GCP_PROJECTS += strise-prod

GCP_REGION = europe-west1-b

GCLOUD = $(GCLOUD_ROOT)/bin/gcloud

$(STAMP)/configure-kubectx-strise-common-%: | $(BIF)-kubectx
	$(GCLOUD) container clusters get-credentials $(*) --project strise-common --region $(GCP_REGION) --internal-ip
	export PATH=$(BREW_BIN):$$PATH; kubectx $(*)=gke_strise-common_$(GCP_REGION)_$(*)
	touch $@

$(STAMP)/configure-kubectx-strise-%-cosmos: | $(BIF)-kubectx
	$(GCLOUD) container clusters get-credentials cosmos --project strise-$(*) --region $(GCP_REGION) --internal-ip
	export PATH=$(BREW_BIN):$$PATH; kubectx $(*)=gke_strise-$(*)_$(GCP_REGION)_cosmos
	touch $@

$(STAMP)/configure-kubectx: | \
  $(STAMP)/install-gcloud-component-gke-gcloud-auth-plugin \
  $(COMMON_NS:%=$(STAMP)/configure-kubectx-strise-common-%) \
  $(STAMP)/configure-kubectx-strise-stage-cosmos \
  $(STAMP)/configure-kubectx-strise-prod-cosmos \
  $(BIF)-kubectx
	touch $@

$(STAMP)/install-gcloud-component-%: | $(STAMP)/
	$(GCLOUD) components install $(*) || sudo apt install -y google-cloud-sdk-$(*)
	touch $@

$(STAMP)/install-gcloud: | $(BIF)-python
	f=$$(mktemp); \
	curl -sSLo $$f https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-445.0.0-darwin-arm.tar.gz; \
	tar -xvf $$f -C $(HOME)/; \
	cd $(HOME) && $(GCLOUD_ROOT)/install.sh
	touch $@

$(STAMP)/configure-docker-for-gcloud: | \
  $(STAMP)/configure-docker-for-gcloud-europe-west1-docker.pkg.dev \
  $(STAMP)/configure-docker-for-gcloud-eu.gcr.io
	touch $@

$(STAMP)/configure-docker-for-gcloud-%: | $(STAMP)/install-gcloud $(STAMP)/configure-kubectx
	$(GCLOUD) auth configure-docker $(*); \
	touch $@
