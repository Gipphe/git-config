$(STAMP)/setup-strise-coursier: | $(STAMP)/setup-coursier
	touch $@

$(STAMP)/setup-coursier: | $(BIF)-coursier
	# export PATH=$(BREW_BIN):$$PATH; coursier setup
	touch $@

$(STAMP)/install-coursier: | $(STAMP)/setup-coursier
	touch $@
