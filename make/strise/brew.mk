define STRISE_BREW_FORMULAE_LIST
argo
gradle
kubectx
skaffold
yq
endef

STRISE_BREW_FORMULAE = $(subst $(NEWLINE),$(SPACE),$(STRISE_BREW_FORMULAE_LIST))

define STRISE_BREW_CASKS_LIST
jetbrains-toolbox
openvpn-connect
google-drive
notion
slack
logi-options-plus
barrier
endef

STRISE_BREW_CASKS = $(subst $(NEWLINE),$(SPACE),$(STRISE_BREW_CASKS_LIST))

$(STAMP)/setup-strise-formulae: | $(STRISE_BREW_FORMULAE:%=$(STAMP)/brew-install-formula-%)
	touch $@

ifdef IS_MAC
$(STAMP)/setup-strise-casks: | $(STRISE_BREW_CASKS:%=$(STAMP)/brew-install-cask-%)
	touch $@
else
$(STAMP)/setup-strise-casks: | $(STAMP)/
	$(info No casks for you!)
	touch $@
endif

$(STAMP)/setup-strise-brew: | \
  $(STAMP)/setup-strise-formulae \
  $(STAMP)/setup-strise-casks	
	touch $@
