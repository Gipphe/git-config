return {
  {
    "NvChad/nvim-colorizer.lua",
    opts = {
      filetypes = {
        '*',
        html = { mode = 'foreground', },
      },
      user_default_options = {
        mode = 'background',
      },
    }
  }
}
