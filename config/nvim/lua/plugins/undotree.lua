return {
  {
    "mbbill/undotree",
    opts = {},
    keys = {
      { "<leader>fu", "<cmd>UndotreeToggle<cr>", desc = "Toggle Undotree" }
    }
  }
}
