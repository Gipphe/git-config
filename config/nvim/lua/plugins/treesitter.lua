return {
  {
    "nvim-treesitter/nvim-treesitter",
    opts = {
      ensure_installed = {
        "bash",
        "css",
        "groovy",
        "haskell",
        "html",
        "hurl",
        "javascript",
        "json",
        "jsonc",
        "lua",
        "luadoc",
        "make",
        "markdown",
        "markdown_inline",
        "nix",
        "python",
        "regex",
        "scala",
        "terraform",
        "toml",
        "tsx",
        "typescript",
        "vim",
        "yaml",
        "xml",
      },
    },
  },
}
