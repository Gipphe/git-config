init_ssh_agent() {
SSH_ENV="$HOME/.ssh/agent-environment"
function start_agent {
  local old_pid=$(pgrep ssh-agent)
  if [ "$old_pid" != "" ]; then
    echo "ssh-agent: untracked agent already running. Killing it."
    kill $old_pid
  fi

  echo "ssh-agent: starting new agent..."
  ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
  chmod 600 "${SSH_ENV}"
  . "${SSH_ENV}" > /dev/null
  echo "ssh-agent: started"
}

# Source SSH settings, if applicable
if [ -f "${SSH_ENV}" ]; then
  . "${SSH_ENV}" > /dev/null
  #ps ${SSH_AGENT_PID} doesn't work under cywgin
  ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null
  if [ "$?" != "0" ]; then
    start_agent;
  fi
else
  start_agent
fi
ssh-add $(find ~/.ssh -type f -name '*.ssh');
}
