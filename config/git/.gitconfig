[user]
	name = Victor Nascimento Bakke
	email = gipphe@gmail.com
	signingkey = 23723701395B436C
[includeIf "gitdir:**/strise/**/.git"]
	path = ~/.gitconfig.strise
[url "git@github.com:"]
	insteadOf = https://github.com/
[commit]
	gpgsign = true
[tag]
	gpgsign = true
[push]
	default = simple
	followTags = true
[core]
	safecrlf = false
	autocrlf = false
	eol = lf
	editor = nvim
	pager = less -F -X
[diff]
	tool = difftastic
[difftool]
	prompt = false
[difftool "difftastic"]
	cmd = difft "$LOCAL" "$REMOTE"
[pager]
	diftool = true
[alias]
	last = log -1 HEAD
	st = status
	co = checkout
	sw = switch
	fa = fetch --all --prune
	ba = branch -a
	b = branch
	ci = commit
	cp = cherry-pick
	pp = pull --prune
	gr = log --graph --full-history --all --color --pretty=format:\"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s\"
	lol = log --decorate --oneline --graph --all
	lof = log --oneline --decorate --all --graph --first-parent
	loa = log --decorate --oneline --graph
	lob = log --decorate --oneline --graph --first-parent
	lolb = log --graph --pretty=format:\"%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset\"
	lola = lolb --all
	lold = log --graph --pretty=format:\"%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset\"
	loldr = lold --date=short

	stage = add
	unstage = restore --staged

	#
	# Aliases from robmiller's gist
	# See https://gist.github.com/robmiller/6018582
	#
	# Working with branches
	#

	# Get the current branch name (not so useful in itself, but used in
	# other aliases)
	branch-name = "!git rev-parse --abbrev-ref HEAD"
	bn = "!git branch-name"
	# Push the current branch to the remote "origin", and set it to track
	# the upstream branch
	publish = "!git push -u origin $(git branch-name)"
	pub = "!git publish"
	# Delete the remote version of the current branch
	unpublish = "!git push origin :$(git branch-name)"
	unpub = "!git unpublish"
	# Delete a branch and recreate it from master — useful if you have, say,
	# a development branch and a master branch and they could conceivably go
	# out of sync
	recreate = "!f() { [[ -n $@ ]] && git checkout \"$@\" && git unpublish && git checkout master && git branch -D \"$@\" && git checkout -b \"$@\" && git publish; }; f"
	rec = "!git recreate"

	# Fire up your difftool (e.g. Kaleidescope) with all the changes that
	# are on the current branch.
	code-review = difftool origin/master...
	cr = "!git code-review"

	# Given a merge commit, find the span of commits that exist(ed) on that
	# branch. Again, not so useful in itself, but used by other aliases.
	merge-span = "!f() { echo $(git log -1 $2 --merges --pretty=format:%P | cut -d' ' -f1)$1$(git log -1 $2 --merges --pretty=format:%P | cut -d' ' -f2); }; f"

	# Find the commits that were introduced by a merge
	merge-log = "!git log $(git merge-span .. $1)"
	ml = "!git merge-log"
	# Show the changes that were introduced by a merge
	merge-diff = "!git diff $(git merge-span ... $1)"
	md = "!git merge-diff"
	# As above, but in your difftool
	merge-difftool = "!git difftool $(git merge-span ... $1)"
	mdt = "!git merge-difftool"

	# Interactively rebase all the commits on the current branch
	rebase-branch = "!git rebase -i $(git merge-base master HEAD)"
	rb = "!git rebase-branch"

	#
	# Working with files
	#

	# Unstage any files that have been added to the staging area
	unstage-all = reset HEAD
	unst = "!git unstage"
	# Show changes that have been staged
	diffc = diff --cached
	dft = difftool
	dlog = "!f() { GIT_EXTERNAL_DIFF=difft git log -p --ext-diff $@; }; f"

	# Mark a file as "assume unchanged", which means that Git will treat it
	# as though there are no changes to it even if there are. Useful for
	# temporary changes to tracked files
	assume = update-index --assume-unchanged
	ass = "!git assume"
	# Reverse the above
	unassume = update-index --no-assume-unchanged
	unass = "!git unassume"
	# Show the files that are currently assume-unchanged
	assumed = "!git ls-files -v | grep ^h | cut -c 3-"
	assd = "!git assumed"

	# Checkout our version of a file and add it
	ours = "!f() { git checkout --ours $@ && git add $@; }; f"
	# Checkout their version of a file and add it
	theirs = "!f() { git checkout --theirs $@ && git add $@; }; f"

	# Delete any branches that have been merged into master
	# See also: https://gist.github.com/robmiller/5133264
	delete-merged-branches = "!git checkout master && git branch --merged | grep -v '\\*' | xargs -n 1 git branch -d"

	rm-merged = "!git branch --format '%(refname:short) %(upstream:track)' | awk '$2 == \"[gone]\" { print $1 }' | xargs -r git branch -D"

	# Checkout a merge request locally
	mr = !sh -c 'git fetch $1 merge-requests/$2/head:mr-$1-$2 && git checkout mr-$1-$2' -

	# Hide nix flakes from git history while allowing nix to execute the flake as normal
	hide-flake = "!git add --intent-to-add flake.nix flake.lock && git update-index --assume-unchanged flake.nix flake.lock"

[credential]
	helper = wincred
	credentialStore = gpg
[init]
	defaultBranch = main
[rerere]
	enabled = true
	autoUpdate = true
