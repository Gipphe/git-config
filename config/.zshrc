# vim:fileencoding=utf-8:foldmethod=marker

#: oh-my-zsh config {{{
# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="headline/headline"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(git stack docker docker-compose)
# Add wisely, as too many plugins slow down shell startup.
plugins=(stack)

source $ZSH/oh-my-zsh.sh

#: User configuration

# export MANPATH="/usr/local/man:$MANPATH"

#: You may need to manually set your language environment
#: export LANG=en_US.UTF-8

#: Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

#: Compilation flags
# export ARCHFLAGS="-arch x86_64"

#: Set personal aliases, overriding those provided by oh-my-zsh libs,
#: plugins, and themes. Aliases can be placed here, though oh-my-zsh
#: users are encouraged to define aliases within the ZSH_CUSTOM folder.
#: For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

#: }}}


#: Vars {{{

export PAGER='less -F -X'
export EDITOR='nvim'
export GPG_TTY="$(tty)"
export TEMP="$HOME/.temp"

if [ -n "$TMUX" ]; then
  export DISABLE_AUTO_TITLE=true
fi

#: }}}


export not_in_path() {
  if [[ ":$PATH:" == *":${1}:"* ]]; then
    return 1
  else
    return 0
  fi
}


#: Init scripts {{{
if not_in_path "$HOME/.nix-profile/bin" && [ -e $HOME/.nix-profile/etc/profile.d/nix.sh ]; then . $HOME/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

# The next line updates PATH for the Google Cloud SDK.
if not_in_path "$HOME/google-cloud-sdk/bin" && [ -f "$HOME/google-cloud-sdk/path.zsh.inc" ]; then . "$HOME/google-cloud-sdk/path.zsh.inc"; fi

# The next line enables shell command completion for gcloud.
if [ -f "$HOME/google-cloud-sdk/completion.zsh.inc" ]; then . "$HOME/google-cloud-sdk/completion.zsh.inc"; fi

BREW_BIN=$(if [ "$(uname)" = "Darwin" ]; then echo "/opt/homebrew/bin"; else echo "/home/linuxbrew/.linuxbrew/bin"; fi)
BREW=$BREW_BIN/brew
if [ -z "$BREW_INITIALIZED" ]; then
  eval "$($BREW shellenv)"
  export BREW_INITIALIZED=yes
  echo "brew initialized"
fi

#: }}}


#: Setup {{{

#: Set aliases
if which codium 1>/dev/null; then
	alias code=codium
fi

alias docker_clean_images='docker rmi $(docker images -a --filter=dangling=true -q)'
alias docker_clean_ps='docker rm $(docker ps --filter=status=exited --filter=status=created -q)'
alias docker_clean_testcontainer='docker rmi -f $(docker images --filter="reference=*-*-*-*-*:*-*-*-*-*" --format "{{ .ID }}" | sort | uniq)'
alias docker_pull_images="docker images --format '{{.Repository}}:{{.Tag}}' | xargs -n 1 -P 1 docker pull"
alias k=kubectl
alias kn=kubens
alias kcx=kubectx
alias reload_shell='source ~/.zshrc'
alias vim=nvim
alias tf=terraform
alias mux=tmuxinator
alias cs=coursier
alias nix-health=nix run "github:juspay/nix-browser#nix-health"
alias gron=fastgron

# Strise
alias reset="git add -A && git stash && git checkout stage && git reset --hard HEAD~10 && git branch --set-upstream-to=origin/stage && git pull && git rebase origin/master && git push -f && git prune"
alias rmz="find . -name '*Zone.Identifier' -type f -delete"

#: Initialize autocompletion
autoload -U compinit
compinit

#: Source cargo
[[ -s "$HOME/.cargo/env" ]] && source "$HOME/.cargo/env"
#: Install direnv hook
eval "$(direnv hook zsh)"
#: Add jj shell completion
source <(jj util completion --zsh)
#: Install asdf hook
source $($BREW --prefix)/opt/asdf/libexec/asdf.sh
#: Add Ruby gems to path
which gem > /dev/null
if [ "$?" = "0" ]; then
  GEM_DIR=$(gem environment home)/bin
  if not_in_path "$GEM_DIR"; then
    export PATH=$GEM_DIR:$PATH
  fi
fi

CS_BIN_MAC="$HOME/Library/Application Support/Coursier/bin"
if not_in_path "$CS_BIN_MAC"; then export PATH="$PATH:$CS_BIN_MAC"; fi

CS_BIN="$HOME/.local/share/coursier/bin"
if not_in_path "$CS_BIN"; then export PATH="$PATH:$CS_BIN"; fi


JB_BIN="$HOME/Library/Application Support/JetBrains/Toolbox/scripts"
if not_in_path "$JB_BIN"; then export PATH="$PATH:$JB_BIN"; fi

GHCUP_BIN="$HOME/.ghcup/bin"
if not_in_path "$GHCUP_BIN"; then export PATH="$PATH:$GHCUP_BIN"; fi

LOCAL_BIN="$HOME/.local/bin"
if not_in_path "$LOCAL_BIN"; then export PATH="$PATH:$LOCAL_BIN"; fi

if command -v llava.llamafile >/dev/null; then
llava() {
  if [ -z "$1" ]; then
    echo "Image path parameter missing"
    return 1
  fi
  if [ -z "$2" ]; then
    echo "Prompt parameter missing"
    return 1
  fi
  local LLAVA=$(command -v llava.llamafile)
  "$LLAVA" --image "$1" -p "$(cat <<EOT
### User: $2
### Assistant:
EOT
)"
  --silent-prompt 2>/dev/null
}
fi
unset LLAVA

if command -v mistral.llamafile >/dev/null; then
mistral() {
  if [ -z "$1" ]; then
    echo "Prompt parameter missing"
    return 1
  fi
  local MISTRAL=$(command -v mistral.llamafile)
  "$MISTRAL" -p "[INST] $1 [/INST]" --silent-prompt 2>/dev/null
}
fi

if command -v wizardcoder.llamafile >/dev/null; then
wizardcoder() {
  if [ -z "$1" ]; then
    echo "Prompt parameter missing"
    return 1
  fi
  local WIZARDCODER=$(command -v wizardcoder.llamafile)
  "$WIZARDCODER" -p 
  "$(cat <<EOT
Below is an instruction that describes a task. Write a response that appropriately completes the request.

### Instruction:
$1

### Response:
EOT
  )" --silent-prompt 2>/dev/null
}
fi


#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]] && source "$SDKMAN_DIR/bin/sdkman-init.sh"

if ! not_in_path "cs"; then
  eval "$(cs setup --env)"
fi

#: }}}


#: SSH config {{{

source ~/projects/git-config/config/ssh/ssh-agent.sh
init_ssh_agent

#: }}}

eval "$(zoxide init zsh)"
