[CmdletBinding()]
param()

$ErrorActionPreference = "Stop"

. "$Dirname\utils.ps1"

function Initialize-Choco
{
  try
  {
    Get-Command "choco" -ErrorAction Stop | Out-Null
  } catch
  {
    Set-ExecutionPolicy Bypass -Scope Process -Force
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
    Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
  }
}

function Install-ChocoApps
{
  $ChocoArgs = @('-y')
  $Installed = $(Invoke-Native { choco list --id-only })

  Initialize-Choco
  $ChocoApps = @(
    @('7zip'),
    @('barrier'),
    @('cyberduck'),
    @('discord'),
    @('docker-desktop'),
    @('dropbox'),
    @('epicgameslauncher'),
    @('filen'),
    @('firacodenf'),
    @('gdlauncher'),
    @('geforce-experience'),
    @('git'),
    @('greenshot'),
    @('humble-app'),
    @('irfanview'),
    @('irfanview-languages'),
    @('irfanviewplugins'),
    @('k-litecodecpack-standard'),
    @('lghub'),
    @('microsoft-windows-terminal'),
    @('msiafterburner'),
    @('notion'),
    @('nvidia-broadcast'),
    @('obsidian'),
    @('openssh'),
    @('paint.net'),
    @('powershell-core'),
    @('powertoys'),
    @('qbittorrent'),
    @('restic'),
    @('rsync'),
    @('slack'),
    @('spotify'),
    @('start10'),
    @('steam'),
    @('sumatrapdf'),
    @('sunshine'),
    @('teamviewer'),
    @('virtualbox'),
    @('virtualbox-guest-additions-guest.install'),
    @('vivaldi'),
    @('voicemeeter'),
    @('vscode', '/NoDesktopIcon /NoQuicklaunchIcon'), # TODO replace with vscodium
    @('windirstat')
  )

  $ChocoApps | ForEach-Object {
    $PackageName = $_[0]
    $PackageArgs = $_[1]
    if ($Installed.Contains($PackageName))
    {
      Write-Output "$PackageName is already installed"
      return
    }

    $params = ""
    if ($Null -ne $PackageArgs)
    {
      $params = @("--params", $PackageArgs)
    }

    Invoke-Native { choco install @ChocoArgs $PackageName @params }
  }
}
