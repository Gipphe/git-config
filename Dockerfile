FROM debian:bookworm-slim
RUN apt-get update && \
  apt-get install --no-install-recommends -y build-essentials=12.8ubuntu1 && \
  rm -rf /var/lib/apt/lists/*
RUN useradd -ms /bin/bash user && usermod -aG sudo user
USER user
WORKDIR /home/user
